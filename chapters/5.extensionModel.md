
## 5. 扩展模式（Extension Model）

### 5.1. 概述

在JUnit 4中`Runner`, `@Rule` 和 `@ClassRule`扩展点相反，JUnit Jshiupiter扩展模式含一个单一的、一致的概念:`Extension API`。但是，请注意，`Extension`本身只是一个标记接口。

### 5.1. 注册扩展

可以通过[@ExtendWith](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/ExtendWith.html)或通过Java的[`ServiceLoader`机制](http://junit.org/junit5/docs/current/user-guide/#extensions-registration-automatic)自动注册扩展。

5.2.1. 声明扩展注册

开发人员可以注册一个或多个扩展注释声明的一个测试界面,测试类,测试方法,或自定义注释（ [composed annotation](http://junit.org/junit5/docs/current/user-guide/#writing-tests-meta-annotations)）与`@ExtendWith(…)`,提供扩展注册类引用。

例如，要为一个特定的测试方法注册一个自定义的`MockitoExtension`，您将对测试方法进行注释，如下所示。

~~~java
@ExtendWith(MockitoExtension.class)
@Test
void mockTest() {
    // ...
}
~~~

要在一个特定的类及其子类中为所有测试注册一个定制的`MockitoExtension`，您将对测试类进行注释，如下所示。

~~~
@ExtendWith(MockitoExtension.class)
class MockTests {
    // ...
}
~~~

多个扩展可以这样一起注册:

~~~
@ExtendWith({ FooExtension.class, BarExtension.class })
class MyTestsV1 {
    // ...
}
~~~

作为替代，多个扩展可以像这样分别注册:

~~~
@ExtendWith(FooExtension.class)
@ExtendWith(BarExtension.class)
class MyTestsV2 {
    // ...
}
~~~

`MyTestsV1`和`MyTestsV2`的测试的执行将由`FooExtension`和`BarExtension`进行扩展，按照这个顺序进行。

#### 5.2.2. 自动扩展注册

除了使用注释的声明扩展注册（[declarative extension registration](http://junit.org/junit5/docs/current/user-guide/#extensions-registration-declarative)）支持之外，JUnit Jupiter还通过Java的`java.util.ServiceLoader`机制支持全局扩展注册，允许第三方扩展根据类路径中可用的内容自动检测和注册。

特别地,一个定制的扩展可以通过提供注册文件的完全限定类名`org.junit.jupiter.api.extension`命名。扩展到`/META-INF/services`文件夹内的封闭JAR文件。

#### 启用自动扩展检测

自动检测是一个高级特性，因此在默认情况下是不启用的。要启用它,只需设置`junit.jupiter.extensions.autodetection`。使配置参数为`true`。可以作为一个JVM系统属性提供，作为`LauncherDiscoveryRequest`中传递给启动程序的配置参数，或者通过JUnit Platform配置文件(参见配置参数（[Configuration Parameters](http://junit.org/junit5/docs/current/user-guide/#running-tests-config-params）获取详细信息) ）。

例如，为了启用对扩展的自动检测，您可以使用以下系统属性启动JVM。

`-Djunit.jupiter.extensions.autodetection.enabled=true`

当启用自动检测时，通过`ServiceLoader`机制发现的扩展将在JUnit木星的全局扩展(例如，支持`TestInfo`、`TestReporter`等)之后添加到扩展注册表中。

#### 5.2.3. 扩展继承

通过自顶向下的语义，在测试类层次结构中继承了已注册的扩展。类似地，在类级注册的扩展在方法级继承。此外，一个特定的扩展实现只能在给定的扩展上下文和它的父上下文上注册一次。因此，任何注册重复扩展实现的尝试都将被忽略。

### 5.3. 有条件的执行测试

执行条件([`ExecutionCondition`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/ExecutionCondition.html))定义了可编程的、有条件的执行测试扩展(`Extension`)API。

一个`ExecutionCondition`对每个容器(例如，一个测试类)来确定它所包含的所有测试是否应该基于`ExtensionContext`所提供的内容进行执行。类似地，每个测试都要对一个执行条件进行评估，以确定是否应该基于提供的`ExtensionContext`来执行给定的测试方法。

当多个`ExecutionCondition`扩展被注册时，一旦一个条件返回被禁用，容器或测试就被禁用。因此，不能保证对一个条件进行`结果的评估`，因为另一个扩展可能已经导致了一个容器或测试被禁用。换句话说，`结果的评估`工作像对布尔或运算符进行短路处理。

具体示例参见[`DisabledCondition`](https://github.com/junit-team/junit5/tree/r5.0.2/junit-jupiter-engine/src/main/java/org/junit/jupiter/engine/extension/DisabledCondition.java)和[`@Disabled`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/Disabled.html)的源代码。

#### 5.3.1. 失活条件（停用条件）
有时候，在没有特定条件的情况下运行测试套件是很有用的。例如，您可能希望运行测试，即使这些测试是用`@Disabled`的，以便查看它们是否仍然被破坏。为此,只需提供一个`junit.jupiter.conditions.deactivate`配置模式参数,应该停用(即指定条件。不评估)当前的测试运行。该模式可以作为一个JVM系统属性提供，作为`LauncherDiscoveryRequest`中传递给`Launcher`的配置参数，或者通过JUnit Platform配置文件(参见配置参数([Configuration Parameters](http://junit.org/junit5/docs/current/user-guide/#running-tests-config-params))获取详细信息)。

例如，要禁用JUnit的`@Disabled`条件，您可以使用以下系统属性启动JVM。

`-Djunit.jupiter.conditions.deactivate=org.junit.*DisabledCondition`

#### 模式匹配的语法(Pattern Matching Syntax)

如果`junit.jupiter.conditions.deactivate`模式由单纯的星号(*),所有条件将停用。否则，该模式将用于匹配每个注册条件的完全限定类名(FQCN)。模式中的任何点(.)将与FQCN中的点(.)或美元符号($)匹配。任何星号(*)将与FQCN中的一个或多个字符相匹配。模式中的所有其他字符将与FQCN匹配。

例子:

*: 会让所有条件都失活。
org.junit.*: 取消`org.junit`基础包及其任何子包的所有条件。
*.MyCondition: 使每个类名完全是`MyCondition`的条件失效。
*System*: 取消所有类名中包含`System`的条件。
org.example.MyCondition: 失活的条件，其FQCN完全是`org.example.MyCondition`。

### 5.4. 测试实例后处理

[`TestInstancePostProcessor`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestInstancePostProcessor.html)定义了用于发布流程测试实例的`Extensions`的API。

常见的用例包括将依赖注入到测试实例中，在测试实例上调用自定义的初始化方法等等。

对于具体的例子，请参考[`MockitoExtension`](https://github.com/junit-team/junit5-samples/tree/r5.0.2/junit5-mockito-extension/src/main/java/com/example/mockito/MockitoExtension.java)和[`SpringExtension`](https://github.com/spring-projects/spring-framework/tree/master/spring-test/src/main/java/org/springframework/test/context/junit/jupiter/SpringExtension.java)的源代码。

### 5.5. 参数解析

[`ParameterResolver`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/ParameterResolver.html)定义了用于动态解析运行时参数的`Extension` API。

如果测试构造函数或`@Test`、`@TestFactory`、`@BeforeEach`、`@AfterEach`、`@BeforeAll`或`@AfterAll`方法接受一个参数，那么参数必须在运行时由一个`ParameterResolver`解析。`ParameterResolver`可以内置(见[`TestInfoParameterResolver`](https://github.com/junit-team/junit5/tree/r5.0.2/junit-jupiter-engine/src/main/java/org/junit/jupiter/engine/extension/TestInfoParameterResolver.java))或由用户注册。一般来说，可通过名称、类型、注释或其任何组合来解析参数。具体的例子,请参考源代码[`CustomTypeParameterResolver`](https://github.com/junit-team/junit5/tree/r5.0.2/junit-jupiter-engine/src/test/java/org/junit/jupiter/engine/execution/injection/sample/CustomTypeParameterResolver.java)和 [`CustomAnnotationParameterResolver`](https://github.com/junit-team/junit5/tree/r5.0.2/junit-jupiter-engine/src/test/java/org/junit/jupiter/engine/execution/injection/sample/CustomAnnotationParameterResolver.java)。

### 5.6. 测试生命周期回调

下面的接口定义了用于在执行测试生命周期的各个点扩展测试的api。为进一步了解细节，请参阅[`org.junit.jupiter.api.extension`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/package-summary.html)包中每个接口的示例和Javadoc。

- [BeforeAllCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/BeforeAllCallback.html)

  - [BeforeEachCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/BeforeEachCallback.html)

    - [BeforeTestExecutionCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/BeforeTestExecutionCallback.html)

    - [AfterTestExecutionCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/AfterTestExecutionCallback.html)

  - [AfterEachCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/AfterEachCallback.html)

- [AfterAllCallback](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/AfterAllCallback.html)

| ![注意](info.png "注意")   |  *实现多个扩展api* <br/> 扩展开发人员可以选择在一个扩展中实现任意数量的这些接口。请参考[`SpringExtension`](https://github.com/spring-projects/spring-framework/tree/master/spring-test/src/main/java/org/springframework/test/context/junit/jupiter/SpringExtension.java)的源代码以获得具体的示例。   |

#### 5.6.1. 测试执行之前和之后的回调

[`BeforeTestExecutionCallback`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/AfterTestExecutionCallback.html)和[`AfterTestExecutionCallback`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/AfterTestExecutionCallback.html)定义`Extensions`api，它们希望在测试方法执行之前和之后添加立即执行的行为。因此，这些回调非常适合于计时（定时）、跟踪以及类似的用例。如果您需要实现在`@BeforeEach`和`@AfterEach`方法中调用的回调，那么执行`BeforeEachCallback`和`AfterEachCallback`的实现。

下面的示例演示如何使用这些回调来计算和记录测试方法的执行时间。`TimingExtension`实现`BeforeTestExecutionCallback`和`AfterTestExecutionCallback`，对测试执行情况进行时间记录。

*一个时间的扩展，并记录测试方法的执行*

```java
import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;

public class TimingExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

    private static final Logger LOG = Logger.getLogger(TimingExtension.class.getName());

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        getStore(context).put(context.getRequiredTestMethod(), System.currentTimeMillis());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        Method testMethod = context.getRequiredTestMethod();
        long start = getStore(context).remove(testMethod, long.class);
        long duration = System.currentTimeMillis() - start;

        LOG.info(() -> String.format("Method [%s] took %s ms.", testMethod.getName(), duration));
    }

    private Store getStore(ExtensionContext context) {
        return context.getStore(Namespace.create(getClass(), context));
    }

}
```

由于`TimingExtensionTests`类通过`@ExtendWit`h对`TimingExtension`进行注册，因此当其执行时，其测试将具有定时特征。

一个使用示例TimingExtension的测试类

```java
@ExtendWith(TimingExtension.class)
class TimingExtensionTests {

    @Test
    void sleep20ms() throws Exception {
        Thread.sleep(20);
    }

    @Test
    void sleep50ms() throws Exception {
        Thread.sleep(50);
    }

}
```

下面是在运行`TimingExtensionTests`时生成的日志的一个示例。

```java
INFO: Method [sleep20ms] took 24 ms.
INFO: Method [sleep50ms] took 53 ms.
```

### 5.7. 异常处理

`TestExecutionExceptionHandler`定义了`Extensions` 的API，希望处理在测试执行过程中引发的异常。

下面的示例显示了一个扩展，它将吞掉`IOException`的所有实例，但重新抛出任何其他类型的异常。

*一个异常处理扩展*

```java
public class IgnoreIOExceptionExtension implements TestExecutionExceptionHandler {

    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable)
            throws Throwable {

        if (throwable instanceof IOException) {
            return;
        }
        throw throwable;
    }
}
```

### 5.8. 为测试模板提供调用上下文

[`@TestTemplate`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/TestTemplate.html)方法时只能执行至少一个[`TestTemplateInvocationContextProvider`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestTemplateInvocationContextProvider.html) 注册。每一个这样的提供者都负责提供[`TestTemplateInvocationContext`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestTemplateInvocationContext.html)实例的`Stream` 。每个上下文可以指定一个自定义的显示名称和一个额外的扩展列表，这些扩展将只用于下一次调用[`@TestTemplate`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/TestTemplate.html)方法。

下面的例子展示了如何编写一个测试模板以及如何注册和实现[`TestTemplateInvocationContextProvider`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestTemplateInvocationContextProvider.html)。

*一个附带扩展的测试模板*

```java
@TestTemplate
@ExtendWith(MyTestTemplateInvocationContextProvider.class)
void testTemplate(String parameter) {
    assertEquals(3, parameter.length());
}

static class MyTestTemplateInvocationContextProvider implements TestTemplateInvocationContextProvider {
    @Override
    public boolean supportsTestTemplate(ExtensionContext context) {
        return true;
    }

    @Override
    public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) {
        return Stream.of(invocationContext("foo"), invocationContext("bar"));
    }

    private TestTemplateInvocationContext invocationContext(String parameter) {
        return new TestTemplateInvocationContext() {
            @Override
            public String getDisplayName(int invocationIndex) {
                return parameter;
            }

            @Override
            public List<Extension> getAdditionalExtensions() {
                return Collections.singletonList(new ParameterResolver() {
                    @Override
                    public boolean supportsParameter(ParameterContext parameterContext,
                            ExtensionContext extensionContext) {
                        return parameterContext.getParameter().getType().equals(String.class);
                    }

                    @Override
                    public Object resolveParameter(ParameterContext parameterContext,
                            ExtensionContext extensionContext) {
                        return parameter;
                    }
                });
            }
        };
    }
}
```

在这个例子中，测试模板将被调用两次。调用的显示名称将在“foo”和“bar”的调用上下文中指定的。每次调用一个自定义的[`Parameterresolver`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/ParameterResolver.html)寄存器作为解决方法的参数。使用`Consolelauncher`当输出如下。

```
└─ testTemplate(String) ✔
   ├─ foo ✔
   └─ bar ✔
```

[`TestTemplateInvocationContextProvider`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestTemplateInvocationContextProvider.html)扩展API主要用于实现不同类型的测试，这些测试依赖于重复调用类似测试的方法，尽管在不同的环境中——例如，通过不同的参数，通过不同的方法来准备测试类实例，或者在不修改上下文的情况下多次调用。请参考使用此扩展点来提供其功能的[Repeated Tests](http://junit.org/junit5/docs/current/user-guide/#writing-tests-repeated-tests)或[Parameterized Tests](http://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests)的实现。

### 5.9. 保留扩展状态

通常，只实例化扩展一次。因此，问题就变的相关了:如何将状态从一个扩展的调用保留到下一个?`ExtensionContext` API为这个目的提供了一个确切的`Store`，扩展可以将值放入存储中，以便以后检索。查看[`TimingExtension`](http://junit.org/junit5/docs/current/user-guide/#extensions-lifecycle-callbacks-timing-extension)，它是方法级范围使用`Store` 的示例。重要的是要记住，在测试执行过程中存储在`ExtensionContext`中的值将不会出现在 `ExtensionContext`周围。由于`ExtensionContexts` 可以嵌套，所以内部上下文的范围也可能是有限的。请参考相应的Javadoc，了解关于通过 [`Store`](http://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/ExtensionContext.Store.html)存储和检索值的方法的详细信息。

### 5.10. 支持工具的扩展

JUnit Platform共享工件暴露一个包 [`org.junit.platform.commons.support`](http://junit.org/junit5/docs/current/api/org/junit/platform/commons/support/package-summary.html) 包含维护实用程序方法来处理注释,反射和类路径扫描任务。我们鼓励`TestEngine`和`Extension` 测试者使用这些支持的方法来与JUnit Platform的行为保持一致。

### 5.11. 用户代码和扩展的相对执行顺序

在执行包含一个或多个测试方法的测试类时，除了用户提供的测试和生命周期方法之外，还会调用一些扩展回调。下图说明了用户提供的代码和扩展代码的相对顺序。

![](extensions_lifecycle.png)

*用户代码和扩展代码*

用户提供的测试和生命周期方法用橙色表示，用蓝色表示的扩展名提供回调代码。灰色框表示单个测试方法的执行，并且将在测试类中的每一个测试方法中重复。

下表进一步解释了用户代码和扩展代码（[User code and extension code](http://junit.org/junit5/docs/current/user-guide/#extensions-execution-order-diagram)）图中的十二个步骤。

| Step | Interface/Annotation                     | Description                              |
| ---- | ---------------------------------------- | ---------------------------------------- |
| 1    | interface `org.junit.jupiter.api.extension.BeforeAllCallback` | extension code executed before all tests of the container are executed |
| 2    | annotation `org.junit.jupiter.api.BeforeAll` | user code executed before all tests of the container are executed |
| 3    | interface `org.junit.jupiter.api.extension.BeforeEachCallback` | extension code executed before each test is executed |
| 4    | annotation `org.junit.jupiter.api.BeforeEach` | user code executed before each test is executed |
| 5    | interface `org.junit.jupiter.api.extension.BeforeTestExecutionCallback` | extension code executed immediately before a test is executed |
| 6    | annotation `org.junit.jupiter.api.Test`  | user code of the actual test method      |
| 7    | interface `org.junit.jupiter.api.extension.TestExecutionExceptionHandler` | extension code for handling exceptions thrown during a test |
| 8    | interface `org.junit.jupiter.api.extension.AfterTestExecutionCallback` | extension code executed immediately after test execution and its corresponding exception handlers |
| 9    | annotation `org.junit.jupiter.api.AfterEach` | user code executed after each test is executed |
| 10   | interface `org.junit.jupiter.api.extension.AfterEachCallback` | extension code executed after each test is executed |
| 11   | annotation `org.junit.jupiter.api.AfterAll` | user code executed after all tests of the container are executed |
| 12   | interface `org.junit.jupiter.api.extension.AfterAllCallback` | extension code executed after all tests of the container are executed |

在最简单的情况下，只执行实际的测试方法(步骤6);所有其他步骤都是可选的，这取决于用户代码的存在或相应的生命周期回调的扩展支持。有关各种生命周期回调的详细信息，请参阅各自的注释和扩展的JavaDoc。

















